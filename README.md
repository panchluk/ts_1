1. Představení testovaného systému (SUT)
2. Představení dokumentu s testovací strategií a testovacími scénáři
3. Představení implementovaných testů ve zvoleném IDE

[Term project/src](https://gitlab.fel.cvut.cz/panchluk/ts_1/-/tree/master/src/main/java/cz/cvut/fel/ts1/term_project?ref_type=heads)

[Term project/wiki](https://gitlab.fel.cvut.cz/panchluk/ts_1/-/wikis/home)
