package cz.cvut.fel.ts1.homework2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class TestCalculator {
    @Test
    public void testAdd() {
        Assertions.assertEquals(3, Calculator.add(1, 2));
        Assertions.assertEquals(0, Calculator.add(0, 0));
        Assertions.assertEquals(0, Calculator.add(-1, 1));
    }
    @Test
    public void testSubtract() {
        Assertions.assertEquals(-1, Calculator.subtract(1, 2));
        Assertions.assertEquals(0, Calculator.subtract(0, 0));
    }
    @Test
    public void testMultiply() {
        Assertions.assertEquals(2, Calculator.multiply(1, 2));
        Assertions.assertEquals(0, Calculator.multiply(0, 0));
        Assertions.assertEquals(-1, Calculator.multiply(-1, 1));
    }
    @Test
    public void testDivide() {
        Assertions.assertEquals(2, Calculator.divide(4, 2));
        Assertions.assertEquals(0, Calculator.divide(0, 1));
        Assertions.assertEquals(-1, Calculator.divide(-1, 1));
    }
    @Test
    public void testDivideByZero() {
        assertThrows(IllegalArgumentException.class, () -> Calculator.divide(1, 0));
    }

}
