package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

// page_url = https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal
@ExtendWith(WebDriverParameterResolver.class)
public class TestIs1FotbalCzClenoveDatabazPage {

    private WebDriver testDriver;
    private WebDriverWait wait;
    private LoginPage loginPage;
    private Home home;

    @BeforeEach
    public void setUp(WebDriver driver) {
        // Set up the driver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\123.0.6312.122\\chromedriver.exe");
        testDriver = driver;
        wait = new WebDriverWait(testDriver, Duration.ofSeconds(10));
        PageFactory.initElements(testDriver, this);
        testDriver.get("https://is1.fotbal.cz");
        home = new Home(testDriver);
    }

    @AfterEach
    public void tearDown() {
        if (testDriver != null) {
            testDriver.quit();
        }
    }

    @Test
    public void testClenoveButton() {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");
    }

    @Test
    public void testRodneCislo() throws InterruptedException {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");

        Is1FotbalCzClenoveDatabazPage page = new Is1FotbalCzClenoveDatabazPage(testDriver);

        page.setRodneCislo("0311290276");

        page.clickSearchButton();

        List<WebElement> results = page.getResults();
        assert results.size() > 0;

        System.out.println(results.get(1).getText());
        assertEquals(results.get(1).getText(), "03111191 Panchartek Lukáš 2003   06.10.2015 06.10.2015 Ano");
        Thread.sleep(5000);
    }

    @Test
    public void testJmeno() {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");

        Is1FotbalCzClenoveDatabazPage page = new Is1FotbalCzClenoveDatabazPage(testDriver);

        page.setJmeno("Marek");
        page.setPrijmeni("Novák");

        page.clickSearchButton();

        List<WebElement> results = page.getResults();
        assert results.size() > 0;
        System.out.println(results.get(1).getText());
        assertEquals(results.get(1).getText(), "00110479 Novák Marek 2000 7240371 - TJ Nedašov z.s. 30.05.2012 21.08.2018 Ne (duplicitní)");

    }

    @Test
    public void testID() {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");

        Is1FotbalCzClenoveDatabazPage page = new Is1FotbalCzClenoveDatabazPage(testDriver);

        page.setIdClenu("73060397");

        page.clickSearchButton();

        List<WebElement> results = page.getResults();
        assert results.size() > 0;

        for (WebElement result : results) {
            System.out.println(result.getText());
        }
        assertEquals(results.get(1).getText(), "Marek Novák 1973 73060397 Novák Marek 1973 2070551 - SK Jivina, z.s. 30.05.2012 17.06.1991 Ano");
    }

    @ParameterizedTest
    @CsvSource({
            "Marek, Novák, 1973, \"73060397 Novák Marek 1973 2070551 - SK Jivina, z.s. 30.05.2012 17.06.1991 Ano\"",
            "Petr, Novák, 1973, 73070040 Novák Petr 1973 5110491 - FK Pertoltice z.s. 30.05.2012 12.03.2001 Ano",
            "Lukáš, Petr, 1985, 85052062 Petr Lukáš 1985 5240091 - SK Dobruška z.s. 30.05.2012 23.08.1995 Ano",
    })
    public void testClenove(String jmeno, String prijmeni, String rokNarozeni, String text) throws InterruptedException {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");

        Is1FotbalCzClenoveDatabazPage page = new Is1FotbalCzClenoveDatabazPage(testDriver);

        page.setJmeno(jmeno);
        page.setPrijmeni(prijmeni);
        page.setRokNarozeni(rokNarozeni);

        page.clickSearchButton();

        List<WebElement> results = page.getResults();
        assert results.size() > 0;

        System.out.println(results.get(1).getText());
        assertEquals(results.get(1).getText(), text);
    }
}