package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(WebDriverParameterResolver.class)
public class DataProviderTest {
    private Home home;
    private WebDriverWait wait;
    private WebDriver testDriver;
    private Is1FotbalCzHraciPrehledHrPage hraci;
    private List<WebElement> seznam;

    @BeforeEach
    public void setUp(WebDriver driver) {
        // Set up the driver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\125.0.6422.78\\chromedriver.exe");

        testDriver = driver;
        wait = new WebDriverWait(testDriver, Duration.ofSeconds(10));
        PageFactory.initElements(testDriver, this);
        testDriver.get("https://is1.fotbal.cz");
        home = new Home(testDriver);
        hraci = new Is1FotbalCzHraciPrehledHrPage(testDriver);
        seznam = new ArrayList<>();
    }

    @AfterEach
    public void tearDown() {
        testDriver.quit();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/HRACI-output.csv")
    public void testClubNameMembership(String club, String name, String membershipStatus, String controlYear) throws InterruptedException {
        // Click on hraci button
        home.clickHraciButton();
        // Choose a club
        hraci.setNazevKlubu(club);
        // Choose a name of the player
        hraci.setJmeno(name);
        // Check the membership status of the player
        hraci.setClenstvi(membershipStatus);
        // Click search button
        hraci.clickSearchButton();
        // Check if the page is correct
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal");
        // Check how many player are in the list / if any player is in the list

        for(WebElement hrac : hraci.getHraci()){
            if (hrac.getText().contains(controlYear)){
                assert hrac.getText().contains(controlYear);
                System.out.println(hrac.getText());
                seznam.add(hrac);
            }
        }
        assert !seznam.isEmpty();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/Hraci2-output.csv")
    public void testSurnameYearGender(String surname, String year, String gender, String controlYear) throws InterruptedException {
        // Click on hraci button
        home.clickHraciButton();
        // Choose a surname of the player
        hraci.setPrijmeni(surname);
        // Choose year of birth
        hraci.setRocnik(year);
        // Choose gender of the player
        hraci.setPohlavi(gender);
        // Click search button
        hraci.clickSearchButton();
        // Check if the page is correct
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal");
        // Check how many player are in the list / if any player is in the list
        for(WebElement hrac : hraci.getHraci()){
            if (hrac.getText().contains(controlYear)){
                System.out.println(hrac.getText());
                seznam.add(hrac);
            }
        }
        assert !seznam.isEmpty();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/Hraci3-output.csv")
    public void testClubNameYearFromYearTo(String club, String name, String yearFrom, String yearTo, String controlYear) throws InterruptedException {
        // Click on hraci button
        home.clickHraciButton();
        // Choose a club
        hraci.setNazevKlubu(club);
        // Choose a name of the player
        hraci.setJmeno(name);
        // Choose range of the year of birth
        hraci.setVekOd(yearFrom);
        hraci.setVekDo(yearTo);
        // Click search button
        hraci.clickSearchButton();
        // Check if the page is correct
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal");
        // Check how many player are in the list / if any player is in the list
        for(WebElement hrac : hraci.getHraci()){
            if (hrac.getText().contains(controlYear)){
                System.out.println(hrac.getText());
                seznam.add(hrac);
            }
        }
        assert !seznam.isEmpty();
    }
}
