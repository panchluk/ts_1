package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(WebDriverParameterResolver.class)
public class TestIs1FotbalCzSoutezePrehledPage {
    private WebDriver testDriver;
    private WebDriverWait wait;
    private Is1FotbalCzSoutezePrehledPage souteze;
    private Home home;

    @BeforeEach
    public void setUp(WebDriver driver) {
        // Set up the driver
        testDriver = driver;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\123.0.6312.122\\chromedriver.exe");
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
        testDriver.get("https://is1.fotbal.cz");
        souteze = new Is1FotbalCzSoutezePrehledPage(testDriver);
        home = new Home(testDriver);
        home.clickSoutezeButton();
    }

    @AfterEach
    public void tearDown() {
        if (testDriver != null) {
            testDriver.quit();
        }
    }

    @Test
    public void testSoutezeButton() {

        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
    }

    @Test
    public void testKonkretniKlub() {
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");

        Is1FotbalCzSoutezePrehledPage page = new Is1FotbalCzSoutezePrehledPage(testDriver);

        page.setNazevKlubu("Slavia Praha");
        page.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal"));

        List<WebElement> results = page.getResults();
        assertEquals(results.size(), 1);

        for (WebElement result : results) {
            assertEquals(result.getText(), "1. liga Slavia Praha");
        }
    }



    @ParameterizedTest
    @CsvSource({
            "E2A",
            "E2B",
            "C3A",
    })
    public void testSouteze(String liga){
        Is1FotbalCzSoutezePrehledPage page = new Is1FotbalCzSoutezePrehledPage(testDriver);


        page.setKod(liga);
        page.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal"));

        List<WebElement> results = page.getResults();
        assertEquals(results.size(), 1);

        for (WebElement result : results) {
            System.out.println(result.getText());
            if(liga.equals("E2A")) {
            }
        }
    }
}
