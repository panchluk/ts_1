package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

// page_url = https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal
@ExtendWith(WebDriverParameterResolver.class)
public class TestIs1FotbalCzHraciPrehledHrPage {

    private WebDriver testDriver;
    private WebDriverWait wait;
    private Is1FotbalCzHraciPrehledHrPage prehled;
    private Home home;

    @BeforeEach
    public void setUp(WebDriver driver) {
        // Set up the driver
        testDriver = driver;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\123.0.6312.122\\chromedriver.exe");
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
        driver.get("https://is1.fotbal.cz");
        prehled = new Is1FotbalCzHraciPrehledHrPage(driver);
        home = new Home(driver);
        home.clickHraciButton();
    }

    @AfterEach
    public void tearDown() {
        if (testDriver != null) {
            testDriver.quit();
        }
    }

    @Test
    public void testHraciButton() {
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal");
    }

    @Test
    public void testHrac(){
        prehled.setJmeno("Marek");
        prehled.setPrijmeni("Novak");
        prehled.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal"));

        assertEquals(prehled.getDatumNarozeni(), 1973);
    }

    @Test
    public void testKlub(){
        prehled.setNazevKlubu("Slavia Praha");
        prehled.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal"));

        assertEquals(prehled.getDatumNarozeni(), 1973);
    }

    @Test
    public void testKlubAndHrac(){
        prehled.setNazevKlubu("Slavia Praha");
        prehled.setJmeno("Marek");
        prehled.setPrijmeni("Novák");
        prehled.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal"));

        assertEquals(prehled.getDatumNarozeni(), 1973);

    }

    @Test
    public void testIDHrace(){
        prehled.setIdClenu("54005412");
        prehled.clickSearchButton();

        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal"));

        assertEquals(prehled.getDatumNarozeni(), 1973);
    }

    @Test
    public void testVekAJmenoAPrijmeni(){
        prehled.setVekOd("40");
        prehled.setVekDo("50");
        prehled.setJmeno("Marek");
        prehled.setPrijmeni("Novak");
        prehled.clickSearchButton();
    }


    @ParameterizedTest
    @CsvSource({
        "Marek, Novak, 1973",
        "Vladimir, Vidim, 1973",
        "Lukas, Kral, 1973",
        "Ladislav, Novak, 1973",
    })
    public void testPrehledForm(String a, String b, int c) {
        // fill the prehled form with csv values

        prehled.setJmeno(a);
        prehled.setPrijmeni(b);
        prehled.clickSearchButton();
        wait.until(ExpectedConditions.urlContains("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal"));
        try {
            // assert that the search results are displayed
            assertEquals(prehled.getDatumNarozeni(), c);
        } catch (NoSuchElementException e) {
            // if the search results are not displayed, print the error message
            System.out.println("No results found");
        }
    }
}