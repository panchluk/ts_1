package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.fail;


@ExtendWith(WebDriverParameterResolver.class)
public class ProcessTest {
            private Home home;
            private Is1FotbalCzSoutezePrehledPage souteze;

            private WebDriverWait wait;
            private WebDriver testDriver;


            @BeforeEach
            public void setUp(WebDriver driver) {
                // Set up the driver
                System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\125.0.6422.78\\chromedriver.exe");

                testDriver = driver;
                wait = new WebDriverWait(testDriver, Duration.ofSeconds(10));
                PageFactory.initElements(testDriver, this);
                testDriver.get("https://is1.fotbal.cz");
                home = new Home(testDriver);
                souteze = new Is1FotbalCzSoutezePrehledPage(testDriver);
            }

            @AfterEach
            public void tearDown() {
                testDriver.quit();
            }

            @Test
            public void testProcessWithValidPath() throws InterruptedException {
                // Click on souteze button
                home.clickSoutezeButton();
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Choose a club
                souteze.setNazevKlubu("SK Slavia Praha");
                Thread.sleep(5000);
                // Click on search button
                souteze.clickSearchButton();
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
                // Click on league
                souteze.clickOnLeague("FORTUNA:LIGA - O TITUL");
                // Click on first match review
                souteze.clickOnMatchReview();
                // Click on print button the review
                try {
                    souteze.clickPrintButton();
                    fail("Expected exception");
                } catch (Exception e) {
                    // RESULT = EXCEPTION
                    System.out.println("Exception thrown");
                }
                // RESULT = CAPTCHA - NEED TO BE SOLVED MANUALLY
            }

            @Test
            public void testProcessWithInvalidData() {
                // Click on souteze button
                home.clickSoutezeButton();
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Choose a club
                souteze.setKod("C1A");
                // Click on search button
                souteze.clickSearchButton();
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
                // Check if the league list is empty
                assert souteze.getResults().isEmpty();
            }

            @Test
            public void testProcessWithValidLogin() throws InterruptedException {
                // Go to login page
                home.clickLoginButton();
                // Login with valid credentials
                LoginPage loginPage = new LoginPage(testDriver);
                loginPage.login("lukypanchartek03@gmail.com", "L1u2k3a4s5@");
                // Check if the page is correct
                Thread.sleep(5000);
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/Default.aspx");
                // Click on souteze button
                home.clickSoutezeButton();
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Choose a club
                souteze.setNazevKlubu("SK Slavia Praha");
                // Click on search button
                souteze.clickSearchButton();
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
                // Click on league
                souteze.clickOnLeague("FORTUNA:LIGA - O ZÁCHRANU");
                // Click on first match review
                souteze.clickOnMatchReview();
                // Click on print button the review
                try {
                    souteze.clickPrintButton();
                    fail("Expected exception");
                } catch (Exception e) {
                    // RESULT = EXCEPTION
                    System.out.println("Exception thrown");
                }
                // RESULT = PRINT PAGE
            }

            @Test
            public void testProcessWithInvalidLogin() throws InterruptedException {
                // Go to login page
                home.clickLoginButton();
                LoginPage loginPage = new LoginPage(testDriver);
                // Login with invalid credentials
                loginPage.login("test", "test");
                Thread.sleep(5000);
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/LogIn.aspx");
                // Go back to main page
                testDriver.get("https://is1.fotbal.cz");
                // Click on souteze button
                home.clickSoutezeButton();
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Choose a club
                souteze.setNazevKlubu("SK Slavia Praha");
                // Click on search button
                souteze.clickSearchButton();
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
                // Click on league
                souteze.clickOnLeague("FORTUNA:LIGA - O ZÁCHRANU");
                // Click on first match review
                souteze.clickOnMatchReview();
                // Click on print button the review
                try {
                    souteze.clickPrintButton();
                    fail("Expected exception");
                } catch (Exception e) {
                    // RESULT = EXCEPTION
                    System.out.println("Exception thrown");
                }
                // RESULT = CAPTCHA - NEED TO BE SOLVED MANUALLY
            }

            @Test
            public void testProcessWithGoingBackToSearch() throws InterruptedException {
                // Go to login page
                home.clickLoginButton();
                // Login with valid credentials
                LoginPage loginPage = new LoginPage(testDriver);
                loginPage.login("lukypanchartek03@gmail.com", "L1u2k3a4s5@");
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/Default.aspx");
                // Click on souteze button
                home.clickSoutezeButton();
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Choose a club
                souteze.setKod("C1A");
                // Click on search button
                souteze.clickSearchButton();
                // Check if the page is correct
                assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
                assert souteze.getResults().isEmpty();
                // Go back to search
                souteze.clickClearButton();
                Thread.sleep(5000);
                // Choose a type of league
                souteze.setDruhSouteze("1.liga (Fotbalová asociace)");
                // Search different club
                souteze.setKod("A1A");
                // Click on search button
                souteze.clickSearchButton();
                // Click on league
                souteze.clickOnLeague("FORTUNA:LIGA");
                // Click on first match review
                souteze.clickOnMatchReview();
                // Click on print button the review
                try {
                    souteze.clickPrintButton();
                    fail("Expected exception");
                } catch (Exception e) {
                    // RESULT = EXCEPTION
                    System.out.println("Exception thrown");
                }
                // RESULT = PRINT PAGE
            }
}
