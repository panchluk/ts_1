package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;


// page_url = https://is1.fotbal.cz/LogIn.aspx
//TODO: UDELAT PRUCHODOVE END TO END TESTY PODLE PROCESNIHO DIAGRAMU, I S INVALID DATY
// TODO: POUZIT 10-15 TESTOVACICH DAT soubor.csv
@ExtendWith(WebDriverParameterResolver.class)
public class TestIs1FotbalCzLoginAspxPage {
    private WebDriver testDriver;
    private WebDriverWait wait;
    private LoginPage loginPage;
    private Home home;

    @BeforeEach
    public void setUp(WebDriver driver) {
        testDriver = driver;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\123.0.6312.122\\chromedriver.exe");
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
        driver.get("https://is1.fotbal.cz");
        loginPage= new LoginPage(driver);
        home = new Home(driver);
        home.clickLoginButton();

    }

    @AfterEach
    public void tearDown() {
        if (testDriver != null) {
            testDriver.quit();
        }
    }

    @Test
    public void loginWithInvalidCredentials() throws InterruptedException {
        Thread.sleep(5000);
        loginPage.login("lukypanchy@fel.cvut.cz", "123456789");
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/LogIn.aspx");

    }

    @Test
    public void loginWithValidCredentials() throws InterruptedException {
        Thread.sleep(5000);
        loginPage.login("lukypanchartek03@gmail.com", "L1u2k3a4s5");
        Thread.sleep(5000);
    }

    @ParameterizedTest
    @CsvSource({
            "lukypanchartek03@gmail.com, L1u2k3a4s5@, https://is1.fotbal.cz/",
            "lukypanchy@fel.cvut.cz, 123456789, https://is1.fotbal.cz/LogIn.aspx",
    })
    public void testLogin(String email, String password, String page) throws InterruptedException {
        // Used only to watch how the fields are filled
        Thread.sleep(5000);
        loginPage.login(email, password);
        Thread.sleep(5000);
        assert testDriver.getCurrentUrl().equals(page);
    }
}