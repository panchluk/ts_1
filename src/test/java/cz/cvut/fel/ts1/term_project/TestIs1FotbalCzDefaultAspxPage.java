package cz.cvut.fel.ts1.term_project;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

// page_url = https://is1.fotbal.cz/default.aspx?sport=fotbal
@ExtendWith(WebDriverParameterResolver.class)
public class TestIs1FotbalCzDefaultAspxPage {

    private WebDriver testDriver;
    private WebDriverWait wait;
    private LoginPage loginPage;
    private Home home;

    @BeforeEach
    public void setUp(WebDriver driver) {
        // Set up the driver
        testDriver = driver;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\123.0.6312.122\\chromedriver.exe");
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
        driver.get("https://is1.fotbal.cz");
        loginPage = new LoginPage(driver);
        home = new Home(driver);
    }

    @AfterEach
    public void tearDown() {
        if (testDriver != null) {
            testDriver.quit();
        }
    }

    @Test
    public void testSoutezeButton() {
        home.clickSoutezeButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/souteze/prehled-soutezi.aspx?sport=fotbal");
    }

    @Test
    public void testHraciButton() {
        home.clickHraciButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal");
    }
    @Test
    public void testLoginButton() {
        home.clickLoginButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/LogIn.aspx");
    }

    @Test
    public void testPodatelnaButton() {
        home.clickPodatelnaButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/podatelna.aspx?sport=fotbal");
    }

    @Test
    public void testClenoveButton() {
        home.clickClenoveButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal");
    }

    @Test
    public void testClenstviButton() {
        home.clickClenstviButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clenove/hromadny-doklad.aspx?sport=fotbal");
    }

    @Test
    public void testLicenceButton() {
        home.clickLicenceButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/kluby/licence-neprofi-kluby.aspx?sport=fotbal");
    }


    @Test
    public void testNapovedaButton() {
        home.clickNapovedaButton();
        assert testDriver.getCurrentUrl().equals("https://is1.fotbal.cz/clanky/vypis-clanku.aspx?sport=fotbal");
    }

    @Test
    public void testFaceBookButton() {
        home.clickFaceBookButton();
        assert testDriver.getCurrentUrl().equals("https://www.facebook.com/ClenstviFacr");
    }


    @Test
    public void testFotbalCzButton() {
        home.clickFotbalCzButton();
        assert testDriver.getCurrentUrl().equals("https://www.fotbal.cz/");
    }
}