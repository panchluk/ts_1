package cz.cvut.fel.ts1.homework4;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import cz.cvut.fel.ts1.homework4.shop.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class TestStandardItem {
    private StandardItem standardItem;

    @BeforeEach
    public void setUp() {
        standardItem = new StandardItem(1, "TestItem", 10000.0F, "TestCategory", 5);
    }

    @Test
    public void testConstructor() {
        assertEquals(1, standardItem.getID());
        assertEquals("TestItem", standardItem.getName());
        assertEquals(10000.0f, standardItem.getPrice());
        assertEquals("TestCategory", standardItem.getCategory());
        assertEquals(5, standardItem.getLoyaltyPoints());
    }

    @Test
    public void testCopy() {
        StandardItem copiedItem = standardItem.copy();
        assertEquals(standardItem.getID(), copiedItem.getID());
        assertEquals(standardItem.getName(), copiedItem.getName());
        assertEquals(standardItem.getPrice(), copiedItem.getPrice());
        assertEquals(standardItem.getCategory(), copiedItem.getCategory());
        assertEquals(standardItem.getLoyaltyPoints(), copiedItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "1, TestItem, 10000.0, TestCategory, 5",
            "2, TestItem2, 20000.0, TestCategory2, 10",
            "3, TestItem3, 30000.0, TestCategory3, 15",
            "4, TestItem4, 40000.0, TestCategory4, 20"
    })
    public void testEquals(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);

        assertEquals(item, item);
        assertEquals(item, standardItem);
    }
}
