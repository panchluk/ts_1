package cz.cvut.fel.ts1.homework4;

import cz.cvut.fel.ts1.homework4.shop.*;
import cz.cvut.fel.ts1.homework4.archive.ItemPurchaseArchiveEntry;
import cz.cvut.fel.ts1.homework4.archive.PurchasesArchive;
import cz.cvut.fel.ts1.homework4.storage.NoItemInStorage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Or;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TestPurchasesArchive {
    private PurchasesArchive purchasesArchive;
    private ItemPurchaseArchiveEntry itemPurchaseArchiveEntry;
    private PurchasesArchive orderArchive;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final String lineSeparator = System.lineSeparator();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemArchiveEntry;
    private Order order;
    private Item item;
    @BeforeEach
    void setUp() {
        purchasesArchive = new PurchasesArchive();

        itemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        orderArchive = mock(PurchasesArchive.class);
        order = mock(Order.class);
        item = mock(Item.class);
    }

    @Test
    void testPutOrderToPurchasesArchiveNotInArchvie() {
        ShoppingCart cart = new ShoppingCart();
        Item item = new StandardItem(1, "Test Item", 100, "Category", 10);
        cart.addItem(item);
        Order order = new Order(cart, "Jan Novák", "Praha, Česká republika");

        purchasesArchive.putOrderToPurchasesArchive(order);

        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    void testPutOrderToPurchasesArchive() {
        ShoppingCart cart1 = new ShoppingCart();
        when(item.getID()).thenReturn(1);
        cart1.addItem(item);


        Order order = new Order(cart1, "Jan Novák", "Praha, Česká republika");

        purchasesArchive.putOrderToPurchasesArchive(order);

        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item));

        ShoppingCart cart2 = new ShoppingCart();
        cart2.addItem(item);

        Order order2 = new Order(cart2, "Jan Novák", "Praha, Česká republika");

        purchasesArchive.putOrderToPurchasesArchive(order2);

        purchasesArchive.printItemPurchaseStatistics();
        assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    void testGetHowManyTimesHasBeenItemSold() {
        when(item.getID()).thenReturn(1);

        assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    void testGetHowManyTimesHasBeenItemSoldWhenItemNotInArchive() {
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(1);
        when(itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);

        itemArchiveEntry = new HashMap<>();
        itemArchiveEntry.put(1, itemPurchaseArchiveEntry);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchiveEntry, null);
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    void testEmptyPrintItemPurchaseStatistics() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        PurchasesArchive purchasesArchive = new PurchasesArchive();

        purchasesArchive.printItemPurchaseStatistics();

        assertEquals("ITEM PURCHASE STATISTICS:" + lineSeparator, outContent.toString());

        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testPrintItemPurchaseStatistics() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        when(itemPurchaseArchiveEntry.toString()).thenReturn("ITEM  "+itemPurchaseArchiveEntry.getRefItem().toString()+"   HAS BEEN SOLD "+itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()+" TIMES");
        purchasesArchive.putOrderToPurchasesArchive(mock(Order.class));
        purchasesArchive.printItemPurchaseStatistics();

        assertEquals("ITEM PURCHASE STATISTICS:" + lineSeparator + "ITEM  null   HAS BEEN SOLD 1 TIMES" + lineSeparator, outContent.toString());

        System.setOut(originalOut);
        System.setErr(originalErr);
    }
}
