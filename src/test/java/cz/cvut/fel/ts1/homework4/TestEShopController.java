package cz.cvut.fel.ts1.homework4;

import cz.cvut.fel.ts1.homework4.archive.PurchasesArchive;
import cz.cvut.fel.ts1.homework4.shop.EShopController;
import cz.cvut.fel.ts1.homework4.shop.Item;
import cz.cvut.fel.ts1.homework4.shop.ShoppingCart;
import cz.cvut.fel.ts1.homework4.shop.StandardItem;
import cz.cvut.fel.ts1.homework4.storage.ItemStock;
import cz.cvut.fel.ts1.homework4.storage.NoItemInStorage;
import cz.cvut.fel.ts1.homework4.storage.Storage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static cz.cvut.fel.ts1.homework4.shop.EShopController.getStorage;
import static org.junit.jupiter.api.Assertions.*;

public class TestEShopController {
    private EShopController controller;
    private ShoppingCart cart;
    private Storage storage;
    private PurchasesArchive purchasesArchive;

    @BeforeEach
    public void setUp() {
        controller = new EShopController();
        cart = new ShoppingCart();
        purchasesArchive = new PurchasesArchive();
        storage = new Storage();
    }

    @Test
    public void shoppingCartOneItemTest() throws NoItemInStorage {
        EShopController.startEShop();
        Item[] items = {
                new StandardItem(1, "Dancing Anakonda", 5000, "GADGETS", 5)
        };
        // Adding item to storage
        EShopController.addItemsToStorage(items[0], 1);


        // Creating a cart
        ShoppingCart cart = EShopController.newCart();
        assertEquals(0, cart.getItemsCount());

        // Adding Item to cart
        cart.addItem(items[0]);
        assertEquals(1, cart.getItemsCount());
        assertTrue(cart.getCartItems().contains(items[0]));

        // Creating an order
        EShopController.purchaseShoppingCart(cart, "Pan Novak", "Praha 1");

        purchasesArchive.printItemPurchaseStatistics();

        // Checking if the item has been sold
        assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(items[0]));
    }

    @Test
    public void shoppingCartTestBuyingWithProductsInStorage() throws NoItemInStorage {
        EShopController.startEShop();

        // Adding items
        int[] itemCount = {1,1,1,1,1};

        Item[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver 1", 200, "TOOLS", 5),
            new StandardItem(4, "Screwdriver 2", 201, "GADGETS", 10),
            new StandardItem(5, "Screwdriver 3", 202, "TOOLS", 5),
        };

        // Adding whit count 1
        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }

        for (int i = 0; i < storageItems.length; i++) {
            assertEquals(1, storage.getItemCount(storageItems[i]));
        }


        ShoppingCart cart = EShopController.newCart();
        assertEquals(0, cart.getItemsCount());

        cart.addItem(storageItems[0]);
        cart.addItem(storageItems[1]);
        cart.addItem(storageItems[2]);

        ArrayList<Item> cartItems = cart.getCartItems();

        assertEquals(3, cart.getItemsCount());

        assertTrue(cart.getCartItems().contains(storageItems[0]));
        assertTrue(cart.getCartItems().contains(storageItems[1]));
        assertTrue(cart.getCartItems().contains(storageItems[2]));

        storage.removeItems(storageItems[1], 1);
        assertEquals(0, storage.getItemCount(storageItems[1]));

        // Buying cart
        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(cart, "Pan Novak", "Praha 1");
        });

        purchasesArchive.printItemPurchaseStatistics();
    }

    @Test
    public void shoppingCartTestNoProductsInStorage() throws NoItemInStorage {
        EShopController.startEShop();

        // Adding items
        int[] itemCount = {};

        Item[] storageItems = {};

        assertEquals(0, storageItems.length);

        ShoppingCart cart = EShopController.newCart();
        cart.addItem(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5));
        assertEquals(1, cart.getItemsCount());

        // Buying cart
        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(cart, "Pan Novak", "Praha 1");
        });

    }
}
