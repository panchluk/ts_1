package cz.cvut.fel.ts1.homework4;

import cz.cvut.fel.ts1.homework4.storage.ItemStock;
import cz.cvut.fel.ts1.homework4.shop.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class TestItemStock {
    // Test constructor, parameterized test of add method, parameterized test of remove method
        public Item item;
    // Test constructor
    @Test
    public void testConstructor() {
        ItemStock itemStock = new ItemStock(item);
        assertNotNull(itemStock);
    }

    // Test increaseItemCount method
    @ParameterizedTest
    @CsvSource({"1, 1", "2, 2", "3, 3"})
    public void testIncreaseItemCount(int x, int expected) {
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(x);
        assertEquals(expected, itemStock.getCount());
    }

    // Test decreaseItemCount method
    @ParameterizedTest
    @CsvSource({"1, 4", "2, 3", "3, 2"})
    public void testDecreaseItemCount(int x, int expected) {
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(5);
        itemStock.decreaseItemCount(x);
        assertEquals(expected, itemStock.getCount());
    }
}
