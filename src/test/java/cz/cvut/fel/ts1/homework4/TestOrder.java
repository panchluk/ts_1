package cz.cvut.fel.ts1.homework4;

import cz.cvut.fel.ts1.homework4.shop.Order;
import cz.cvut.fel.ts1.homework4.shop.ShoppingCart;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestOrder {
    private ShoppingCart cart;


    @BeforeEach
    public void setUp() {
        // Set up the test environment
        cart = new ShoppingCart();
    }

    // Test constructors
    @Test
    public void testConstructorWithoutState() {
        Order order = new Order(cart, "Jan Novák", "Praha, Česká republika");

        assertEquals("Jan Novák", order.getCustomerName());
        assertEquals("Praha, Česká republika", order.getCustomerAddress());
        assertEquals(0, order.getState());
        assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void testConstructorWithState() {
        Order order = new Order(cart, "Jan Novák", "Praha, Česká republika", 1);

        assertEquals("Jan Novák", order.getCustomerName());
        assertEquals("Praha, Česká republika", order.getCustomerAddress());
        assertEquals(1, order.getState());
        assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void testNullCart() {
        assertThrows(NullPointerException.class, () -> {
            new Order(null, "Jan Novák", "Praha, Česká republika");
        });
    }
}
