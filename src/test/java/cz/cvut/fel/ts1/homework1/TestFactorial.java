package cz.cvut.fel.ts1.homework1;

import org.junit.jupiter.api.Test;
public class TestFactorial {
    @Test
    public void testFactorial() {
        assert Factorial.factorial(0) == 1;
        assert Factorial.factorial(1) == 1;
        assert Factorial.factorial(2) == 2;
        assert Factorial.factorial(3) == 6;
        assert Factorial.factorial(4) == 24;
    }
    @Test
    public void testFactorialNegative() {
        try {
            assert Factorial.factorial(-1) == 1;
        } catch (IllegalArgumentException e) {
            assert true;
        }
    }

}
