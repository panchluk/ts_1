package cz.cvut.fel.ts1.homework8;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(WebDriverParameterResolver.class)
public class LinkSpringerTest {


    private WebDriverWait wait;
    private LinkSpringerMainPage home;
    private LinkSpringerSignUpLogin login;
    private WebDriver testDriver;


    public static final String URL = "https://link.springer.com/";

    @BeforeEach
    public void setUp(WebDriver driver) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\hulkp\\.cache\\selenium\\chromedriver\\win64\\125.0.6422.78\\chromedriver.exe");

        testDriver = driver;
        wait = new WebDriverWait(testDriver, Duration.ofSeconds(10));
        PageFactory.initElements(testDriver, this);
        driver.get(URL);
        home = new LinkSpringerMainPage(testDriver);
        login = new LinkSpringerSignUpLogin(testDriver);

    }

    @AfterEach
    public void tearDown() {
        testDriver.quit();
    }


    @Test
    public void testAcceptCookies() throws InterruptedException {
        acceptCookies();
        home.loginButton();

        login.login();

    }

    @ParameterizedTest
    @CsvSource({
            "'Simultaneous Multi-View Object Recognition and Grasping in Open-Ended Domains', '16 April 2024', 'https://doi.org/10.1007/s10846-024-02092-5'",
            "'Improving Android app exploratory testing with UI test cases using code change analysis', '04 April 2024', 'https://doi.org/10.1007/s11334-024-00555-4'",
            "'A reinforcement learning-based approach to testing GUI of moblie applications', '30 January 2024', 'https://doi.org/10.1007/s11280-024-01252-9'",
            "'Histone 3 Trimethylation Patterns are Associated with Resilience or Stress Susceptibility in a Rat Model of Major Depression Disorder', '16 January 2024', 'https://doi.org/10.1007/s12035-024-03912-3'"
    })
    public void testEquals(String query, String testDate, String testDOI) throws InterruptedException {
        acceptCookies();
        home.loginButton();

        login.login();

        testDriver.get(URL);
        try {
            acceptCookies();
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        search(query);
        List<String> articles = getArticles();
        if (articles == null || articles.isEmpty()) {
            System.out.println("Articles not found");
            return;
        }
        String link = articles.getFirst();
        testDriver.get(link);
        String title = articleTitle();
        String date = articlePublicationDate();
        String DOI = articleDOI();

        assertEquals(query, title);
        assertEquals(testDate, date);
        assertEquals(testDOI, DOI);

    }
    private void acceptCookies() {
        try {
            WebElement acceptCookies = testDriver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept"));
            acceptCookies.click();
        } catch (NoSuchElementException e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
            System.out.println("Cookies not found");
        }
    }
    private void search(String query) {
        try {
            WebElement search = testDriver.findElement(By.cssSelector("#homepage-search"));
            search.sendKeys(query);
            search.submit();
        } catch (NoSuchElementException e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
            System.out.println("Search not found");
        }
    }

    @FindBy(css = "h1[data-test='article-title']")
    private WebElement articleTitle;

    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li:nth-child(3) > p > span.c-bibliographic-information__value > time")
    private WebElement articlePublicationDate;

    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--full-width > p > span.c-bibliographic-information__value")
    private WebElement articleDOI;

    @FindBy(className = "app-card-open__link")
    private List<WebElement> articleLinks;

    public List<String> getArticles() {
        List<String> links = new ArrayList<>();
        try {
            for (WebElement article : articleLinks) {
                links.add(article.getAttribute("href"));
            }
        } catch (NoSuchElementException e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
            System.out.println("Articles not found");
        }
        return links;
    }

    public String articleTitle() {
        try {
            wait.until(ExpectedConditions.visibilityOf(articleTitle));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        return articleTitle != null ? articleTitle.getText() : null;
    }

    public String articlePublicationDate() {
        try {
            wait.until(ExpectedConditions.visibilityOf(articlePublicationDate));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        return articlePublicationDate != null ? articlePublicationDate.getText() : null;
    }

    public String articleDOI() {
        try {
            wait.until(ExpectedConditions.visibilityOf(articleDOI));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        return articleDOI != null ? articleDOI.getText() : null;
    }
}