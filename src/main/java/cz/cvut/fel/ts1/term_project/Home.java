package cz.cvut.fel.ts1.term_project;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

// page_url = https://is1.fotbal.cz/?sport=fotbal
public class Home {
    private WebDriver driver;
    private WebDriverWait wait;

    public Home(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @FindBy(css = "#TopMenu_dvNotLoged > p > a")
    private WebElement loginButton;
    @FindBy(css = "#TopMenu_aSouteze")
    private WebElement soutezeButton;
    @FindBy(css = "#TopMenu_liSeznamHracu")
    private WebElement hraciButton;
    @FindBy(css = "#TopMenu_linkPodatelna")
    private WebElement podatelnaButton;
    @FindBy(css = "#TopMenu_aLicence")
    private WebElement licenceButton;
    @FindBy(css = "#TopMenu_A5")
    private WebElement clenoveButton;
    @FindBy(css = "#TopMenu_A7")
    private WebElement clenstviButton;
    @FindBy(css = "#TopMenu_linkFakturace")
    private WebElement fakturaceButton;
    @FindBy(css = "#TopMenu_A8")
    private WebElement napovedaButton;
    @FindBy(css = "#divFooter > div > ul > li > a:nth-child(1)")
    private WebElement faceBookButton;
    @FindBy(css = "#divFooter > div > ul > li > a:nth-child(2)")
    private WebElement clenstviFotbalButton;
    @FindBy(css = "#divFooter > div > ul > li > a:nth-child(3)")
    private WebElement fotbalCzButton;

    void clickLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(loginButton)).click();
    }
    void clickSoutezeButton() {
        wait.until(ExpectedConditions.elementToBeClickable(soutezeButton)).click();
    }

    void clickHraciButton() {
        wait.until(ExpectedConditions.elementToBeClickable(hraciButton)).click();
    }

    void clickPodatelnaButton() {
        wait.until(ExpectedConditions.elementToBeClickable(podatelnaButton)).click();
    }

    void clickLicenceButton() {
        wait.until(ExpectedConditions.elementToBeClickable(licenceButton)).click();
    }

    void clickClenoveButton() {
        wait.until(ExpectedConditions.elementToBeClickable(clenoveButton)).click();
    }

    void clickClenstviButton() {
        wait.until(ExpectedConditions.elementToBeClickable(clenstviButton)).click();
    }

    void clickFakturaceButton() {
        wait.until(ExpectedConditions.elementToBeClickable(fakturaceButton)).click();
    }

    void clickNapovedaButton() {
        wait.until(ExpectedConditions.elementToBeClickable(napovedaButton)).click();
    }

    void clickFaceBookButton() {
        wait.until(ExpectedConditions.elementToBeClickable(faceBookButton)).click();
    }

    void clickClenstviFotbalButton() {
        wait.until(ExpectedConditions.elementToBeClickable(clenstviFotbalButton)).click();
    }

    void clickFotbalCzButton() {
        wait.until(ExpectedConditions.elementToBeClickable(fotbalCzButton)).click();
    }
}