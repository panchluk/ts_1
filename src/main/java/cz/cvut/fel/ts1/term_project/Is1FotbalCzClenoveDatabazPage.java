package cz.cvut.fel.ts1.term_project;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;


// page_url = https://is1.fotbal.cz/clenove/databaze-clenu.aspx?sport=fotbal
public class Is1FotbalCzClenoveDatabazPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public Is1FotbalCzClenoveDatabazPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#ctl00_MainContent_OddilBoxClenem_txtCisloKlubu")
    private WebElement cisloKlubu;
    @FindBy(css = "#ctl00_MainContent_OddilBoxClenem_txtNazevKlubu")
    private WebElement nazevKlubu;
    @FindBy(css = "#MainContent_txtSearchRc")
    private WebElement rodneCislo;
    @FindBy(css = "#MainContent_txtSearchPrijmeni")
    private WebElement prijmeni;
    @FindBy(css = "#MainContent_txtSearchJmeno")
    private WebElement jmeno;
    @FindBy(css = "#MainContent_txtSearchRocnik")
    private WebElement rokNarozeni;
    @FindBy(css= "#MainContent_listSearchOverenyEmail")
    private WebElement emailState;
    @FindBy(css= "#MainContent_txtEmailClena")
    private WebElement email;
    @FindBy(css = "#MainContent_txtSearchIdClenu")
    private WebElement idClenu;
    @FindBy(css = "#MainContent_btnSearch")
    private WebElement searchButton;
    @FindBy(css = "#MainContent_btnClear")
    private WebElement clearButton;
    @FindBy(css = "#MainContent_VypisClenu1_gridData > tbody > tr")
    private List<WebElement> results;
    @FindBy(css = "#ui-id-2 > li:nth-child(1) > a:nth-child(1)")
    private WebElement confirmKlubButton;

    void setCisloKlubu(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(cisloKlubu)).sendKeys(text);
    }

    void setNazevKlubu(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(nazevKlubu)).sendKeys(text);
    }

    void setRodneCislo(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(rodneCislo)).sendKeys(text);
    }

    void setJmeno(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(jmeno)).sendKeys(text);
    }

    void setPrijmeni(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(prijmeni)).sendKeys(text);
    }

    void setRokNarozeni(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(rokNarozeni)).sendKeys(text);
    }

    void setEmailState(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(emailState)).sendKeys(text);
    }

    void setEmail(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(email)).sendKeys(text);
    }

    void setIdClenu(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(idClenu)).sendKeys(text);
    }

    void clickSearchButton() {
        wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
    }

    void clickClearButton() {
        wait.until(ExpectedConditions.elementToBeClickable(clearButton)).click();
    }

    public List<WebElement> getResults() {
        wait.until(ExpectedConditions.visibilityOfAllElements(results));
        return results;
    }

    public void setConfirmKlubButton() {
        wait.until(ExpectedConditions.elementToBeClickable(confirmKlubButton)).click();
    }
}