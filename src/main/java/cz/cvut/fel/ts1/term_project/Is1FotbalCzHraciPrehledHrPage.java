package cz.cvut.fel.ts1.term_project;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

// page_url = https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal
public class Is1FotbalCzHraciPrehledHrPage {
    private WebDriverWait wait;

    public Is1FotbalCzHraciPrehledHrPage(WebDriver driver) {
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }
    static String URL = "https://is1.fotbal.cz/hraci/prehled-hracu.aspx?sport=fotbal";
    // Číslo klubu (Club Number)
    @FindBy(id = "ctl00_MainContent_OddilBoxClenem_txtCisloKlubu")
    private WebElement cisloKlubu;

    // Název klubu (Club Name)
    @FindBy(id = "ctl00_MainContent_OddilBoxClenem_txtNazevKlubu")
    private WebElement nazevKlubu;

    // Rodné číslo (Birth Number)
    @FindBy(id = "MainContent_txtSearchRc")
    private WebElement rodneCislo;

    // Příjmení (Surname)
    @FindBy(id = "MainContent_txtSearchPrijmeni")
    private WebElement prijmeni;

    // Jméno (First Name)
    @FindBy(id = "MainContent_txtSearchJmeno")
    private WebElement jmeno;

    // ID člena nebo ID členů (Member ID or IDs)
    @FindBy(id = "MainContent_txtSearchIdClenu")
    private WebElement idClenu;

    // Věk od (Age From)
    @FindBy(id = "MainContent_txtOd")
    private WebElement vekOd;

    // Věk do (Age To)
    @FindBy(id = "MainContent_txtDo")
    private WebElement vekDo;

    // Ročník (Year)
    @FindBy(id = "MainContent_txtRocnik")
    private WebElement rocnik;

    // Pohlaví (Gender)
    @FindBy(id = "listPohlavi")
    private WebElement pohlavi;

    // Členství (Membership)
    @FindBy(id = "listClen")
    private WebElement clenstvi;

    // Foto (Photo)
    @FindBy(id = "listFoto")
    private WebElement foto;

    // Typ výpisu (Type of Listing)
    @FindBy(id = "listTypVypisu")
    private WebElement typVypisu;

    // Vyhledat button (Search Button)
    @FindBy(id = "btnSearch")
    private WebElement searchButton;

    // Zruš button (Clear Button)
    @FindBy(id = "MainContent_btnClear")
    private WebElement clearButton;

    // Datum narozeni - found
    @FindBy(css = "#MainContent_VypisHracu_gridData > tbody > tr.last > td:nth-child(6)")
    private WebElement datumNarozeni;

    // Jméno - found
    @FindBy(css = "#MainContent_VypisHracu_gridData > tbody > tr")
    private List<WebElement> hraci;

    int getDatumNarozeni() {
        return Integer.parseInt(datumNarozeni.getText());
    }
    void setCisloKlubu(String text) {
        cisloKlubu.sendKeys(text);
    }

    void setNazevKlubu(String text) {
        nazevKlubu.sendKeys(text);
    }

    void setRodneCislo(String text) {
        rodneCislo.sendKeys(text);
    }

    void setPrijmeni(String text) {
        (prijmeni).sendKeys(text);
    }

    void setJmeno(String text) {
        wait.until(ExpectedConditions.visibilityOf(jmeno)).sendKeys(text);
    }

    void setIdClenu(String text) {
        idClenu.sendKeys(text);
    }

    void setVekOd(String text) {
        vekOd.sendKeys(text);
    }

    void setVekDo(String text) {
        vekDo.sendKeys(text);
    }

    void setRocnik(String text) {
        rocnik.sendKeys(text);
    }

    void setPohlavi(String text) {
        pohlavi.sendKeys(text);
    }

    void setClenstvi(String text) {
        clenstvi.sendKeys(text);
    }

    void setFoto(String text) {
        foto.sendKeys(text);
    }

    void setTypVypisu(String text) {
        typVypisu.sendKeys(text);
    }

    void clickSearchButton() {
        wait.until(ExpectedConditions.visibilityOf(searchButton)).click();
    }

    void clickClearButton() {
        clearButton.click();
    }

    public List<WebElement> getHraci() {
        return hraci;
    }
}
