package cz.cvut.fel.ts1.term_project;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

// page_url = https://is1.fotbal.cz/LogIn.aspx
public class LoginPage {
    public WebDriverWait wait;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @FindBy(css = "#txtLoginClient")
    private WebElement usernameInput;
    @FindBy(css = "#txtPsw")
    private WebElement passwordInput;
    @FindBy(css = "#btnLogin > span")
    private WebElement loginButton;
    @FindBy(css = "#btnLogout > span")
    private WebElement rememberMeButton;
    @FindBy(css = "#form1 > div.outer > div > div.form > fieldset > table > tbody > tr:nth-child(5) > td > a")
    private WebElement forgotPasswordButton;
    @FindBy(css = "#form1 > div.outer > div > div.header > div > a")
    private WebElement logo;

    public void clickLogo() {
        logo.click();
    }

    public void clickForgotPasswordButton() {
        forgotPasswordButton.click();
    }
    public void clickRememberMeButton() {
        rememberMeButton.click();
    }

    public void login(String username, String password) throws InterruptedException {

        wait.until(ExpectedConditions.visibilityOf(usernameInput)).sendKeys(username);
        passwordInput.sendKeys(password);

        loginButton.click();
    }

}