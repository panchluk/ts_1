package cz.cvut.fel.ts1.term_project;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Is1FotbalCzSoutezePrehledPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public Is1FotbalCzSoutezePrehledPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='listSearchDruhSouteze']")
    private WebElement druhSouteze;
    @FindBy(css = "#ctl00_MainContent_bxOrgJednotka_txtNazevOrgJednotky")
    private WebElement orgJednotka;
    @FindBy(css = "#ui-id-11 > li")
    private List<WebElement> orgJednotkaSeznam;
    @FindBy(css = "#ctl00_MainContent_OddilBoxClenem_txtCisloKlubu")
    private WebElement cisloKlubu;
    @FindBy(css = "#ctl00_MainContent_OddilBoxClenem_txtNazevKlubu")
    private WebElement nazevKlubu;
    @FindBy(css = "#ui-id-10 > li")
    private List<WebElement> nazevKlubuSeznam;
    @FindBy(css = "#MainContent_txtSearchNazev")
    private WebElement nazev;
    @FindBy(css = "#listSearchRocnik")
    private WebElement rocnik;
    @FindBy(css = "#MainContent_txtSearchKod")
    private WebElement kod;
    @FindBy(css = "#txtSearchCislo")
    private WebElement cislo;
    @FindBy(css = "#btnSearch")
    private WebElement searchButton;
    @FindBy(css = "#MainContent_btnClear")
    private WebElement clearButton;
    @FindBy(css = "#MainContent_gridData > tbody > tr > td > a")
    private List<WebElement> results;
    @FindBy(css = "#MainContent_rptKolaSouteze_rptZapasy_0_linkZapis_0")
    private WebElement matchReview;
    @FindBy(css = "#ContentPlaceHolderMaster_ZapisSouhrn1_panelPrint > div > input[type=button]")
    private WebElement printButton;
    @FindBy(css = "#recaptcha-anchor > div.recaptcha-checkbox-border")
    private WebElement recaptcha;
    @FindBy(css = "#MainContent_btnLogin")
    private WebElement continueButton;

    void setDruhSouteze(String text) {
        Select select = new Select(druhSouteze);
        select.selectByVisibleText(text);
    }

    void setSoutez(String text) {
        WebElement select = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#l#MainContent_ctl00 > div > div.col.combine.f-left > div > fieldset > table > tbody > tr.first.odd > td > span > span"))));
        select.sendKeys("1.liga (Fotbalová asociace)");
    }

    void setOrgJednotka(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(orgJednotka)).sendKeys(text);
        for (WebElement orgJednotkaLink : getOrgJednotkaSeznam()) {
            System.out.println(orgJednotkaLink.getText());
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", orgJednotkaLink);
            wait.until(ExpectedConditions.elementToBeClickable(orgJednotkaLink)).click();
            break;
        }

    }
    List<WebElement> getNazevKlubuSeznam() {
        return nazevKlubuSeznam;
    }

    List<WebElement> getOrgJednotkaSeznam() {
        return orgJednotkaSeznam;
    }

    void setCisloKlubu(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(cisloKlubu)).sendKeys(text);
    }

    void setNazevKlubu(String text) {
        Actions actions = new Actions(driver);
        actions.moveToElement(nazevKlubu).click().sendKeys(text).sendKeys(Keys.BACK_SPACE).sendKeys(Keys.PAGE_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
    }

    void setNazev(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(nazev)).sendKeys(text);
    }

    void setRocnik(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(rocnik)).sendKeys(text);
    }

    void setKod(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(kod)).sendKeys(text);
    }

    void setCislo(String text) {
        wait.until(ExpectedConditions.elementToBeClickable(cislo)).sendKeys(text);
    }

    void clickSearchButton() {
        wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
    }

    void clickClearButton() {
        wait.until(ExpectedConditions.elementToBeClickable(clearButton)).click();
    }

    public List<WebElement> getResults() {
        return results;
    }


    public void clickOnLeague(String leagueName) {
        for (WebElement leagueLink : getResults()) {
            if (leagueLink.getText().contains(leagueName)) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", leagueLink);
                wait.until(ExpectedConditions.elementToBeClickable(leagueLink)).click();
                break;
            }
        }
    }

    public void clickOnMatchReview() {
        wait.until(ExpectedConditions.elementToBeClickable(matchReview)).click();
    }

    public void clickPrintButton() {
        wait.until(ExpectedConditions.elementToBeClickable(printButton)).click();
    }
}
