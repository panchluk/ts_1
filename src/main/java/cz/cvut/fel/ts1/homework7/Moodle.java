package cz.cvut.fel.ts1.homework7;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.List;

// page_url = https://moodle.fel.cvut.cz
public class Moodle {
    @FindBy(css = "input#username")
    private static WebElement usernameField;
    @FindBy(css = "input#password")
    private static WebElement passwordField;
    @FindBy(css = "[type='submit']")
    private static WebElement submitButton;

    @FindBy(css = "html > body > div:nth-of-type(2) > div:nth-of-type(2) > div > div:nth-of-type(2) > div > section > div > aside > section:nth-of-type(2) > div > div > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > div > a")
    private static WebElement courseLink2;
    @FindBy(css = "html > body > div:nth-of-type(2) > div:nth-of-type(2) > div > div:nth-of-type(2) > div > section > div > div > div > ul > li:nth-of-type(5) > div:nth-of-type(2) > ul > li:nth-of-type(16) > div > div:nth-of-type(2) > div > div > div > div > a")
    private static WebElement testRobotLink;
    @FindBy(css = "[type='submit']")
    private static WebElement startTestButton;

    @FindBy(xpath = "//a[contains(@class, 'py-2')]")
    public static WebElement linkLog;
    @FindBy(css = "[type='submit']")
    public static WebElement attemptButton;
    @FindBy(xpath = "/html/body/div[4]/div[3]/div/div[2]/form/div[2]/div[2]/fieldset/div/div[1]/span/input")
    public static WebElement attemptButton2;
    @FindBy(css = "[id$='_answer_id']")
    public static WebElement textArea;
    @FindBy(css = "[id$='_answer']")
    public static WebElement input;
    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div[2]/div/section/div[2]/form/div/div[3]/div[2]/div[1]/div/p/span/select")
    public static WebElement select;
    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div[2]/div/section/div[2]/form/div/div[4]/div[2]/div[1]/div/p/span/select")
    public static WebElement select2;
    @FindBy(css = "a[href='https://moodle.fel.cvut.cz/mod/quiz/summary.php?attempt=139892&cmid=308513']")
    public static WebElement nextButton;
    @FindBy(css = "button[type='button']")
    public static WebElement hideButton;
    @FindBy(xpath = "/html/body/div[2]/header/div/div[2]/div/div[1]/div[1]/div/div/div/div/div/a")
    public static WebElement profileButton;
    @FindBy(xpath = "/html/body/div[2]/header/div/div[2]/div/div[1]/div[1]/div/div/div/div/div/a")
    public static WebElement logoutButton;
    @FindBy(xpath = "/html/body/div[2]/header/div/div[1]/a/i[1]")
    public static WebElement reviewButton;
    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div[2]/div/section/div/div/div/div[3]/div/div[2]/form/button")
    public static WebElement submitButton2;

    public Moodle(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://moodle.fel.cvut.cz");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));


        WebElement loginButton = driver.findElement(By.cssSelector("a[href='https://moodle.fel.cvut.cz/login/index.php']"));
        wait.until(ExpectedConditions.visibilityOf(loginButton)).click();

        WebElement ssoButton = driver.findElement(By.cssSelector("[data-cy='login-sso-a']"));
        wait.until(ExpectedConditions.visibilityOf(ssoButton)).click();

        Moodle page = new Moodle(driver);
        wait.until(ExpectedConditions.visibilityOf(page.usernameField)).sendKeys("");
        wait.until(ExpectedConditions.visibilityOf(page.passwordField)).sendKeys("");
        page.submitButton.click();


        courseLink2.click();
        testRobotLink.click();
        startTestButton.click();

        try {
            driver.findElement(By.cssSelector("[type='submit']")).click();
            System.out.println("Submit clicked");
        } catch (ElementClickInterceptedException e) {
            attemptButton2.click();
            System.out.println("ElementClickInterceptedException");
        }

        wait.until(ExpectedConditions.visibilityOf(textArea)).sendKeys("Lukas Panchartek 101");

        wait.until(ExpectedConditions.visibilityOf(input)).sendKeys(Integer.toString(24 * 60 * 60));

        try {
            wait.until(ExpectedConditions.visibilityOf(select));
            if (select.isEnabled()) {
                Select selectEle = new Select(select);
                selectEle.selectByVisibleText("Oberon");
                System.out.println("Select element is enabled.");
            } else {
                System.out.println("Select element is not enabled.");
            }
        } catch (TimeoutException e) {
            System.out.println("Timeout waiting for select element to be visible.");
        }

        try {
            wait.until(ExpectedConditions.visibilityOf(select2));
            if (select2.isEnabled()) {
                Select selectEle = new Select(select2);
                selectEle.selectByVisibleText("Rumunsko");
            } else {
                System.out.println("Select2 element is not enabled.");
            }
        } catch (TimeoutException e) {
            System.out.println("Timeout waiting for select2 element to be visible.");
        }

        wait.until(ExpectedConditions.urlContains("https://moodle.fel.cvut.cz/mod/quiz/review.php?attempt"));

        driver.get("https://moodle.fel.cvut.cz/login/logout.php");
        wait.until(ExpectedConditions.urlToBe("https://moodle.fel.cvut.cz/login/logout.php"));

        wait.until(ExpectedConditions.visibilityOf(submitButton2)).click();

        driver.quit();
        }

}