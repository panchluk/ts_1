package cz.cvut.fel.ts1.homework8;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

// page_url = https://link.springer.com/advanced-search
public class LinkSpringerAdvancedSearch {
    public LinkSpringerAdvancedSearch(WebDriver driver) {
        LinkSpringerAdvancedSearch.driver = driver;
        PageFactory.initElements(driver, this);
    }
    static String URL = "https://link.springer.com/advanced-search";
    private static WebDriver driver = Main.driver;
    private static final WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    // All words
    @FindBy(css = "#all-words")
    private static WebElement allWordsInput;
    // Some words
    @FindBy(css = "#least-words")
    private static WebElement someWordsInput;
    // Select IN
    @FindBy(css = "#date-facet-mode")
    private static WebElement selectIn;
    // Year input
    @FindBy(css = "#facet-start-year")
    private static WebElement yearInput;
    // Search button
    @FindBy(css = "#submit-advanced-search")
    private static WebElement searchButton;

    public static void allWords(String query) {
        // Search for the query
        wait.until(ExpectedConditions.visibilityOf(allWordsInput));
        allWordsInput.sendKeys(query);
    }
    public static void someWords(String query) {
        // Search for the query
        wait.until(ExpectedConditions.visibilityOf(someWordsInput));
        someWordsInput.sendKeys(query);
    }
    public static void selectIn(String query) {
        // Search for the query
        wait.until(ExpectedConditions.visibilityOf(selectIn));
        selectIn.sendKeys(query);
    }
    public static void year(String query) {
        // Search for the query
        wait.until(ExpectedConditions.visibilityOf(yearInput));
        yearInput.sendKeys(query);
    }
    public static void search() {
        // Search for the query
        wait.until(ExpectedConditions.visibilityOf(searchButton)).click();
    }
}