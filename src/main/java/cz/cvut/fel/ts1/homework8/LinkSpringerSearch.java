package cz.cvut.fel.ts1.homework8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

// page_url = https://link.springer.com/search
public class LinkSpringerSearch {
    public LinkSpringerSearch(WebDriver driver) {
        LinkSpringerSearch.driver = driver;
        PageFactory.initElements(driver, this);
    }
    static String URL = "https://link.springer.com/search";
    private static WebDriver driver = Main.driver;
    private static final WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    // Filters button
    @FindBy(xpath = "//button[@data-track-action='See filters']")
    private static WebElement filtersButton;
    @FindBy(xpath = "//label[@for='content-type-article']")
    private static WebElement articlesInput;
    // Custom dates button
    @FindBy(css = "label[for='date-custom']")
    private static WebElement customDates;
    // Start date input
    @FindBy(css = "#date-from")
    private static WebElement startDate;
    // End date input
    @FindBy(css = "#date-to")
    private static WebElement endDate;
    // Update results button
    @FindBy(css = "button[data-track-label^='update']")
    private static WebElement updateButton;

    static void articlesButton() {
        try {
            ScrollUtils.scrollToElement(driver, articlesInput);
            wait.until(ExpectedConditions.visibilityOf(articlesInput));
            wait.until(ExpectedConditions.elementToBeClickable(articlesInput));
            articlesInput.click();

        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());

        }
    }
    static void customDatesButton() {
        try {
            wait.until(ExpectedConditions.visibilityOf(customDates)).click();
        } catch (Exception e) {
            System.out.println("Custom dates already selected");
        }
    }
    static void startDateInput(String date) {
        try {
            wait.until(ExpectedConditions.visibilityOf(startDate));
            startDate.sendKeys(date);
        } catch (Exception e) {
            System.out.println("Start date already filled");
        }

    }
    static void endDateInput(String date) {
        endDate.sendKeys(date);
    }
    static void updateButton() {
        updateButton.click();
    }
    public static void filtersButton() {
        // Click on the filters button
        try {
            // Wait until the filters button is visible and clickable
            wait.until(ExpectedConditions.visibilityOf(filtersButton));
            wait.until(ExpectedConditions.elementToBeClickable(filtersButton));

            // Check if the filters button is enabled before clicking it
            if (filtersButton.isEnabled()) {
                filtersButton.click();
            } else {
                System.out.println("Filters button is not enabled");
            }
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
    }
    }
    // Articles
    @FindBy(css = "html > body > div:nth-of-type(4) > div > div:nth-of-type(3) > div > div:nth-of-type(2) > div:nth-of-type(2) > ol")
    private static List<WebElement> articles;
    // Click on the first article
    @FindBy(css = "html > body > div:nth-of-type(4) > div > div:nth-of-type(3) > div > div:nth-of-type(2) > div:nth-of-type(2) > ol > li:nth-of-type(1) > div:nth-of-type(1) > div > h3 > a[data-track-label*='title']")
    private static WebElement firstArticle;
    // Click on the second article
    @FindBy(css = "html > body > div:nth-of-type(4) > div > div:nth-of-type(3) > div > div:nth-of-type(2) > div:nth-of-type(2) > ol > li:nth-of-type(2) > div:nth-of-type(1) > div > h3 > a")
    private static WebElement secondArticle;
    // Click on the third article
    @FindBy(css = "html > body > div:nth-of-type(4) > div > div:nth-of-type(3) > div > div:nth-of-type(2) > div:nth-of-type(2) > ol > li:nth-of-type(3) > div:nth-of-type(1) > div > h3 > a")
    private static WebElement thirdArticle;
    // Click on the fourth article
    @FindBy(css = "html > body > div:nth-of-type(4) > div > div:nth-of-type(3) > div > div:nth-of-type(2) > div:nth-of-type(2) > ol > li:nth-of-type(4) > div:nth-of-type(1) > div > h3 > a")
    private static WebElement fourthArticle;

    @FindBy(className = "app-card-open__link")
    private static List<WebElement> articleLinks;

    // Get first four articles
    public static List<String> getArticles() {
        List<String> links = null;
        try {
            links = new ArrayList<>();
            for (WebElement article : articleLinks) {
                links.add(article.getAttribute("href"));
            }
        } catch (NoSuchElementException e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
            System.out.println("Articles not found");
        }
        return links;
    }


    public void citeArticleButton() {
        // Click on the cite button
        try {
            WebElement citeButton = driver.findElement(By.cssSelector("a[data-track-action*='this']"));
            wait.until(ExpectedConditions.visibilityOf(citeButton));
            wait.until(ExpectedConditions.elementToBeClickable(citeButton));
            citeButton.click();
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
    }
    public void clickNoThanksButton() {
        try {
            WebElement noThanksButton = driver.findElement(By.xpath("//button[text()='No Thanks']"));
            noThanksButton.click();
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
    }
}