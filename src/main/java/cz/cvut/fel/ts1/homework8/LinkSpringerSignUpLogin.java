package cz.cvut.fel.ts1.homework8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


// page_url = https://link.springer.com/signup-login
public class LinkSpringerSignUpLogin {
    public LinkSpringerSignUpLogin(WebDriver driver) {
        LinkSpringerSignUpLogin.driver = driver;
        PageFactory.initElements(driver, this);
    }

    static String URL = "https://link.springer.com/signup-login";
    static WebDriver driver;
    static WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    @FindBy(css = "a[href='/signup']")
    static WebElement signUpButton;
    @FindBy(css = "input[aria-required='true']")
    static WebElement loginField;
    @FindBy(css = "button[data-track^='form']")
    static WebElement continueButton;
    @FindBy(css = "input[data-test='givenNames']")
    static WebElement nameInput;
    @FindBy(css = "input[data-test='familyName']")
    static WebElement surnameInput;
    @FindBy(css = "#registration-password")
    static WebElement passwordInput;
    @FindBy(css = "#registration-password-confirm")
    static WebElement passwordConfirmInput;
    @FindBy(css = "label[for='registration-terms-conditions']")
    static WebElement termsCheckbox;
    @FindBy(css = "button[data-track^='form']")
    static WebElement createAccountButton;

    @FindBy(css = "#login-password")
    static WebElement passwordInputLogin;
    static String password = "lukas123456789";

    static void loginEmail() {
        // Login
        wait.until(ExpectedConditions.visibilityOf(loginField)).sendKeys("panchluk@fel.cvut.cz");
    }

    static void continueButton() {
        wait.until(ExpectedConditions.visibilityOf(continueButton)).click();
    }

    static void nameFill() {
        wait.until(ExpectedConditions.visibilityOf(nameInput)).sendKeys("Petr");
    }

    static void surnameFill() {
        wait.until(ExpectedConditions.visibilityOf(surnameInput)).sendKeys("Panchluk");
    }

    static String passwordGenerator() {
        String password = "";
        for (int i = 0; i < 7; i++) {
            password = password + (char) (Math.random() * 26 + 'a') + i;
        }
        return password;
    }

    static void passwordFill() {
        wait.until(ExpectedConditions.visibilityOf(passwordInput)).sendKeys(password);
    }

    static void passwordConfirmFill() {
        wait.until(ExpectedConditions.visibilityOf(passwordConfirmInput)).sendKeys(password);
    }

    static void termsCheckbox() {
        wait.until(ExpectedConditions.visibilityOf(termsCheckbox)).click();
    }

    static void createAccountButton() {
        wait.until(ExpectedConditions.visibilityOf(createAccountButton)).click();
    }

    static void loginPassword() {
        wait.until(ExpectedConditions.visibilityOf(passwordInputLogin)).sendKeys(password);
    }
    // SignUp
    public static void signUp() {
        loginEmail();
        continueButton();
        nameFill();
        surnameFill();
        passwordFill();
        passwordConfirmFill();
        termsCheckbox();
        createAccountButton();
    }

    // Login
    public static void login() {
        loginEmail();
        continueButton();
        loginPassword();
        continueButton();
    }
}