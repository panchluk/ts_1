package cz.cvut.fel.ts1.homework8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

// page_url = https://link.springer.com/
public class LinkSpringerMainPage {
    public static final String URL = "https://link.springer.com/";

    // WebDriver
    private static WebDriver driver;
    // Wait for the element to be visible
    private static WebDriverWait wait;
    // Accept cookies button
    private static WebElement acceptCookiesButton;
    // Login button
    private static WebElement loginButton;
    // Input queries in the search bar
    private static WebElement searchInput;
    // Search button
    private static WebElement searchButton;

    public LinkSpringerMainPage(WebDriver driver) {
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public static void acceptCookies() {
        // Accept cookies
        try {
            acceptCookiesButton = driver.findElement(By.cssSelector("button[data-cc-action='accept']"));
            wait.until(ExpectedConditions.visibilityOf(acceptCookiesButton)).click();
        } catch (Exception e) {
            System.out.println("Cookies already accepted");
        }
    }
    public static void loginButton() {
    // Click on the login button
        loginButton = driver.findElement(By.cssSelector("span[class='eds-c-header__widget-fragment-title']"));
        wait.until(ExpectedConditions.visibilityOf(loginButton)).click();
    }
    public static void search(String query) {
        // Search for the query
        searchInput = driver.findElement(By.cssSelector("input[class*='app-homepage-hero']"));
        wait.until(ExpectedConditions.visibilityOf(searchInput));
        searchInput.sendKeys(query);
        try {
            // Attempt to dismiss common overlays or banners
            WebElement consentDismiss = driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept"));
            consentDismiss.click();
        } catch (Exception e) {
            System.out.println("No consent banner found or not dismissible.");

        }
        searchButton = driver.findElement(By.cssSelector("button[class*='app-homepage-hero']"));
        wait.until(ExpectedConditions.visibilityOf(searchButton)).click();
    }
}