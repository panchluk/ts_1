package cz.cvut.fel.ts1.homework8;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static cz.cvut.fel.ts1.homework8.LinkSpringerSearch.getArticles;

// page_url = https://link.springer.com/article/
public class LinkSpringerArticle {
    public LinkSpringerArticle(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    static String URL = "https://link.springer.com/article/";
    private static WebDriver driver = Main.driver;
    private static final WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    // Title of the articles
    @FindBy(css = "h1[data-test='article-title']")
    private static WebElement articleTitle;
    // Publication date of the articles
    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li:nth-child(3) > p > span.c-bibliographic-information__value > time")
    private static WebElement articlePublicationDate;
    // DOI of the articles
    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--full-width > p > span.c-bibliographic-information__value")
    private static WebElement articleDOI;


    public void printArticles() {
        List<String> articles = getArticles();
        if (articles == null) {
            System.out.println("Articles not found");
            return;
        }
        if (articles.size() < 4) {
            System.out.println("Less than 4 articles found");
            return;
        }
        for (int i = 0; i < 4; i++) {
            String link = articles.get(i);
            driver.get(link);
            String title = articleTitle();
            String date = articlePublicationDate();
            String DOI = articleDOI();
            printArticleDetails(title, date, DOI);
            driver.navigate().back();
        }
    }
    // Print article details
    public void printArticleDetails(String title, String date, String DOI) {
        System.out.println(title);
        System.out.println(date);
        System.out.println(DOI);
    }
    public static String articleTitle() {
        try {
            ScrollUtils.scrollToElement(driver, articleTitle);
            wait.until(ExpectedConditions.visibilityOf(articleTitle));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        if (articleTitle == null) {
            System.out.println("Title not found");
            return null;
        }
        return articleTitle.getText();
    }
    // Get the publication date of the articles
    public static String articlePublicationDate() {
        try {
            ScrollUtils.scrollToElement(driver, articlePublicationDate);
            wait.until(ExpectedConditions.visibilityOf(articlePublicationDate));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        if (articlePublicationDate == null) {
            System.out.println("Publication date not found");
            return null;
        }
        return articlePublicationDate.getText();
    }
    // Get the DOI of the articles
    public static String articleDOI() {
        try {
            ScrollUtils.scrollToElement(driver, articleDOI);
            wait.until(ExpectedConditions.visibilityOf(articleDOI));
        } catch (Exception e) {
            System.out.println("Caught exception of type: " + e.getClass().getName());
        }
        if (articleDOI == null) {
            System.out.println("DOI not found");
            return null;
        }
        return articleDOI.getText();
    }
}