package cz.cvut.fel.ts1.homework8;

import cz.cvut.fel.ts1.homework8.LinkSpringerSearch;
import cz.cvut.fel.ts1.homework8.LinkSpringerArticle;
import cz.cvut.fel.ts1.homework8.LinkSpringerAdvancedSearch;
import cz.cvut.fel.ts1.homework8.LinkSpringerSignUpLogin;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static cz.cvut.fel.ts1.homework8.LinkSpringerSearch.*;

public class Main {
    /*
    7. domácí úkol - Selenium (7 bodů)
    termín odevzdání: začátek cvičení v 13. týdnu, tj. 15/16.5 dle rozvrhu

    Odevzdávejte do svého repozitáře pod adresář ts1-selenium


    Vytvořte nový maven projekt ve kterém připravte Selenium Page Object Model třídy s pomocí Page Factory které budou pracovat se stránkou https://link.springer.com/.

    Připravte Page Object Model pro stránky:

    https://link.springer.com/    -k přesunu na přihlašovací obrazovku

    https://link.springer.com/signup-login    -k přihlášení

    https://link.springer.com/advanced-search     -pro pokročilé vyhledávání, tedy práci s formulářem

    https://link.springer.com/search    -pro procházení výsledků vyhledávání

    https://link.springer.com/article/     -pro čtení článku

    Připravte program, který vyhledá na link.springer.com všechny články(Content type = Article) obsahující slova “Page Object Model” a alespoň jedním ze slov “Sellenium Testing” publikovaných v tomto roce. Poté přečte první čtyři články a uložte si jejich Název, DOI a datum publikace. Napište parametrizované testy pomocí vytvořených Page Object Modelů, které Vás přihlásí jako uživatele a vyhledávají uložené články (pomocí jejich názvu) a zkontrolují, že odpovídá jejich datum publikace a DOI.

    Zamyslete se nad znovupoužitelností kódu pro zjednodušení práce.

     */
    static boolean logged = false;
    static WebDriver driver = new ChromeDriver();
    static LinkSpringerMainPage lsmp = new LinkSpringerMainPage(driver);
    static LinkSpringerSearch lss = new LinkSpringerSearch(driver);
    static LinkSpringerArticle lsa = new LinkSpringerArticle(driver);
    static LinkSpringerAdvancedSearch lsas = new LinkSpringerAdvancedSearch(driver);
    static LinkSpringerSignUpLogin lssl = new LinkSpringerSignUpLogin(driver);

    public static void main(String[] args) throws InterruptedException {
        //LinkSpringerSignUpLogin.login();
        //searchArticles();
    }

    static void login(){
        // Create new account
        driver.get(LinkSpringerMainPage.URL);

        LinkSpringerMainPage.acceptCookies();
        LinkSpringerMainPage.loginButton();

        LinkSpringerSignUpLogin.login();
        LinkSpringerSignUpLogin.continueButton();
        LinkSpringerMainPage.acceptCookies();
        LinkSpringerSignUpLogin.nameFill();
        LinkSpringerSignUpLogin.surnameFill();
        LinkSpringerSignUpLogin.passwordFill();
        LinkSpringerSignUpLogin.passwordConfirmFill();
        LinkSpringerSignUpLogin.termsCheckbox();
        LinkSpringerSignUpLogin.createAccountButton();

        // Login
        logged = true;


    }
    static void searchArticles() throws InterruptedException {
        // Search for articles
        driver.get(LinkSpringerAdvancedSearch.URL);


        lsmp.acceptCookies();

        LinkSpringerAdvancedSearch.allWords("Page Object Model");
        LinkSpringerAdvancedSearch.someWords("Selenium Testing");
        LinkSpringerAdvancedSearch.selectIn("in");
        LinkSpringerAdvancedSearch.year("2024");
        LinkSpringerAdvancedSearch.search();

        LinkSpringerSearch.filtersButton();
        LinkSpringerSearch.articlesButton();
        LinkSpringerSearch.customDatesButton();
        LinkSpringerSearch.startDateInput("2024");
        LinkSpringerSearch.endDateInput("2024");
        LinkSpringerSearch.updateButton();

        lss.clickNoThanksButton();

        lsa.printArticles();

        Thread.sleep(10000);
        driver.quit();
    }


}